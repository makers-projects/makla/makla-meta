# makla-meta

This repo contains general resources regarding *makla*, the **m**usically **a**ware **k**eyboard-**l**ook-**a**like. As a first step, this readme will serve as a description of the main idea and basic concepts.

# The Basic Idea

In very short, *makla* is a musical instrument trying to overcome major downsides of traditional keyboard instruments by analyzing and processing the performed music in real-time. For a more detailed view, let us have a look at...

## The Shortcomings of Traditional Keyboard Instruments

### Voice Leading

In traditional music ensembles, each instrument plays one voice, i.e. mostly one note at a time, of a polyphonic setting.
For instance, this can be a violin in a string quartet, or a singer in a choir.
This leads to the voices being coherent within themselves and distinguishable from each other.
Instruments with the inherent ability to produce different sound colors, e.g. the violin with its multiple strings, usually try to achieve a homogeneous sound color among a musical line.

Traditional keyboard instruments need to compromise in this regard: Each note is played on a different string or set of strings in the case of a piano, and on a different pipe on an organ.
Instrument makers try matching the sound colors of neighboring notes and thereby making voices sound coherent.
Yet, the sounds colors of neighboring keys can never match completely, and there are inherent reasons for significant mismatches, such as the piano using different numbers of strings for different notes.
Furthermore, deliberate contrasts between the voices are cannot be achieved.
While a pianist can e.g. emphasize a melodic line by playing it louder, they can not significantly influence its sound color or location within the room.

### Pitch Fine-Tuning

All traditional western music (on which we are focusing here) gets its notes by dividing each octave (i.e. the frequency space between a base frequency and its double) into 12 steps.
In a perfect musical world, the frequency of each of these 12 steps is derived from the overtone series of the base note.
This is because playing a note yields not only a base frequency (except in the case of an artificial sinusoidal wave) but also a number of additional frequencies, the harmonics.
Having multiple, simultaneously played notes aligned in terms of their harmonics leads to an harmonic overall sound.

However, the exact frequencies needed for this alignment depend on the exact combination of notes being played.
Therefore, when fixing the frequencies of each note, e.g. in a traditional keyboard instrument, decisions have to be made:
When aligning all frequencies in regard to a base key, certain harmonics align perfectly, while others are significantly off.
Some historic temperaments try to achieve some good alignments while avoiding serious mismatches.

Today, we almost always use *equal temperament*, which divides the octave into 12 equal steps.
This leads to no intervals being seriously off, but also no interval except the octave being perfectly aligned.

## The Solution

While historic instrument makers have gone to great lengths trying to compensate for above shortcomings and have achieved impressive partial solutions, e.g. instruments with multiple keyboards, the core problem as a whole can only be tackled by identifying the played harmonics and voices.
While theoretically there may be possibilities of achieving this by mechanical solutions or having the performer intervene, the most straightforward way in this day and age is by using an electrical setup and processing the musical input with software that tries to identify the harmonics and voices and applies pitch fine-tuning and voice mapping.

This concept additionally allows for deliberately partial or alternative mitigations of above shortcomings, e.g. using various historical temperaments instead of perfect fine-tuning.
Furthermore, it can even go beyond the abilities of traditional ensembles in these regards, e.g. by placing voices in far-away corners of a concert hall or using unconventional sources of sound like tesla coils.


Until I get to describing the technical implementation, have a bunch of pictures of the current progress:

[![Keys](https://johannes-kuenel.de/static/img/makla/small/keys.jpg "Keys")](https://johannes-kuenel.de/static/img/makla/big/keys.jpg)

[![Keys, detail view of the contacts](https://johannes-kuenel.de/static/img/makla/small/keys-contacts.jpg "Keys, detail view of the contacts")](https://johannes-kuenel.de/static/img/makla/big/keys-contacts.jpg)

[![Processing and audio PCB mounted](https://johannes-kuenel.de/static/img/makla/small/pcbs-assembled.jpg "Processing and audio PCB mounted")](https://johannes-kuenel.de/static/img/makla/big/pcbs-assembled.jpg)

[![Auio PCB](https://johannes-kuenel.de/static/img/makla/small/pcb-audio.jpg "Audio PCB")](https://johannes-kuenel.de/static/img/makla/big/pcb-audio.jpg)

[![Processing PCB](https://johannes-kuenel.de/static/img/makla/small/pcb-processing.jpg "Processing PCB")](https://johannes-kuenel.de/static/img/makla/big/pcb-processing.jpg)

[![Control panel](https://johannes-kuenel.de/static/img/makla/small/panel.jpg "Control panel")](https://johannes-kuenel.de/static/img/makla/big/panel.jpg)
